#!/bin/bash 

### 
# Script to check if XRebirth is launched 
# and set digital vibrance accordingly 
# This script is intended: 
# ***ONLY*** for NVIDIA propietary drivers 
# check your ideal digital vibrance values 
# in the game before using this script. 

# Author: ezra-r 
# Version: 29-08-2016 

set -e 

### 
# Set vibrance values 
desktopvib=100 
xrebirthvib=900 


############################ 
# no need to modify bellow # 
############################ 

### 
# binaries and checks 
rebirth="XRebirth" 

if [ "$(ps auxw | grep $USER | grep -v grep| grep "./${rebirth}")" != "" ]; then 
   echo "This game is already running!" 
   echo "Use this script to launch the game" 
   echo "If it is already launched, stop the other instance first." 
   echo "" 
   exit 1 
fi 

nvset="/usr/bin/nvidia-settings" 
valvesteam="/usr/games/steam" 

### 
# Main functionality 

# run steam 
$valvesteam steam://rungameid/2870 

# wait for game to run 
sleep 20s 

# check game is running and set vibrance 
if [ $(ps auxw | grep "./${rebirth}" | grep -v grep | awk '{print $2}') ]; then 
   $nvset -a "DigitalVibrance=${xrebirthvib}" 
else 
   echo "Game is not launched!" 
   echo "Just in case we print the process list matching ${rebirth}:" 
   echo "$(ps aux | grep "./${rebirth}" | grep -v grep | awk '{print $2}')" 
   echo "" 
   exit 1 
fi 

# while in game we check game still running 
# when game is shutdown we reset desktop vibrance 
while [ $(ps aux | grep "./${rebirth}" | grep -v grep | awk '{print $2}') ]; do 
   sleep 10s 
done 

$nvset -a "DigitalVibrance=${desktopvib}" 

exit 0 